package com.intern.broadcastreceiver;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;

public class MainActivity extends AppCompatActivity {

    private static final int PERMISSION_CODE = 21,PHONESTATECCODE=20;
    TabLayout tbl;
    String CalllogPermission= Manifest.permission.READ_CALL_LOG;
    String PhoneStatePermission=Manifest.permission.READ_PHONE_STATE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }
    private void init() {
        tbl=findViewById(R.id.tablayout);
        clicklistners();
    }

    private void clicklistners() {
        tbl.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                // Toast.makeText(MainActivity.this, tab.getText(), Toast.LENGTH_SHORT).show();
                if(tab.getText().equals("Call Logs"))
                {
                    //Toast.makeText(MainActivity.this, "Call Logs", Toast.LENGTH_SHORT).show();
                    if(CheckPermission())
                    {
                        //code to executes
                    }

                }
                else
                {
                    //Toast.makeText(MainActivity.this, "Messages", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                if(CheckPermission())
                {
                    //code to executes
                }
            }
        });
    }

    private boolean CheckPermission()
    {
        if( ActivityCompat.checkSelfPermission(this,PhoneStatePermission)== PackageManager.PERMISSION_GRANTED)
        {
           return true;
        }
        else
        {
            ActivityCompat.requestPermissions(this, new String[]{PhoneStatePermission},PHONESTATECCODE);
            return false;

        }

    }


}