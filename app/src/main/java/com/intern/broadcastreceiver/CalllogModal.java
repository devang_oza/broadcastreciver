package com.intern.broadcastreceiver;

public class CalllogModal {
    String phnumber,contactName,callType,callDate,callTime,callDuration;

    public CalllogModal(String phnumber, String contactName, String callType, String callDate, String callTime, String callDuration) {
        this.phnumber = phnumber;
        this.contactName = contactName;
        this.callType = callType;
        this.callDate = callDate;
        this.callTime = callTime;
        this.callDuration = callDuration;
    }

    public String getPhnumber() {
        return phnumber;
    }

    public String getContactName() {
        return contactName;
    }

    public String getCallType() {
        return callType;
    }

    public String getCallDate() {
        return callDate;
    }

    public String getCallTIme() {
        return callTime;
    }

    public String getCallDuration() {
        return callDuration;
    }
}
