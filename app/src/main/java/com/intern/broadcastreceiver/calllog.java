package com.intern.broadcastreceiver;

import android.Manifest;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.provider.CallLog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Formatter;
import java.util.List;
import java.util.Locale;


public class calllog extends Fragment {
    private static final int PERMISSION_CODE = 21;
    String CalllogPermission= Manifest.permission.READ_CALL_LOG;
    private ArrayList<CalllogModal> call_logs;
    CallLogAdapter calllogAdapter;
    RecyclerView rv_activity_main_logs;
    SwipeRefreshLayout Swipe_logs;

    String strNumber,str_contact_name,str_call_type,
            str_Date_full,str_Date,str_Duration,str_call_time,strCallTimeFormatted,
    str_call_duration;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_calllog, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(CheckPermission())
        {
            init(view);
            FetchCallLogs();
        }
    }

    private void FetchCallLogs()
    {
        String sortOrder= CallLog.Calls.DATE + " DESC";
        Cursor cursor=getContext().getContentResolver().query(
                CallLog.Calls.CONTENT_URI,null,null,
                null,sortOrder);
        call_logs.clear();
        while (cursor.moveToNext())
        {
            strNumber=cursor.getString(cursor.getColumnIndex(CallLog.Calls.NUMBER));
            str_contact_name=cursor.getString(cursor.getColumnIndex(CallLog.Calls.CACHED_NAME));
            str_contact_name= str_contact_name==null || str_contact_name.equals("") ? "UnKnown" : str_contact_name;
            str_call_type=cursor.getString(cursor.getColumnIndex(CallLog.Calls.TYPE));
            str_Date_full=cursor.getString(cursor.getColumnIndex(CallLog.Calls.DATE));
            str_Duration=cursor.getString(cursor.getColumnIndex(CallLog.Calls.DURATION));

            SimpleDateFormat dateFormatter=new SimpleDateFormat("dd-mm-yyyy");
            str_Date=dateFormatter.format(new Date(Long.parseLong(str_Date_full)));

            SimpleDateFormat timeFormatter=new SimpleDateFormat("HH:mm:ss");
            str_call_time=timeFormatter.format(new Date(Long.parseLong(str_Date_full)));

            strCallTimeFormatted=getFormateDateTime(str_call_time,"HH:mm:ss","hh:mm a");
            str_Duration=DurationFormate(str_Duration);

            switch (Integer.parseInt(str_call_type))
            {
                case CallLog.Calls.OUTGOING_TYPE:
                    str_call_type="OUTGOING";
                    break;
                case CallLog.Calls.INCOMING_TYPE:
                    str_call_type="INCOMING";
                    break;
                case CallLog.Calls.MISSED_TYPE:
                    str_call_type="MISSED";
                    break;
                case CallLog.Calls.VOICEMAIL_TYPE:
                    str_call_type="Voice Mail";
                    break;
                case CallLog.Calls.REJECTED_TYPE:
                    str_call_type="Rejected";
                    break;
                case CallLog.Calls.BLOCKED_TYPE:
                    str_call_type="Blocked";
                    break;
                case CallLog.Calls.ANSWERED_EXTERNALLY_TYPE:
                    str_call_type="Externally Answered";
                    break;
                default:
                    str_call_type="NA";
                    break;
            }
            CalllogModal calllogModal=new CalllogModal(strNumber,str_contact_name,str_call_type,str_Date,strCallTimeFormatted,str_Duration);
            call_logs.add(calllogModal);
        }
        calllogAdapter.notifyDataSetChanged();
    }

    private String DurationFormate(String str_duration)
    {
        String Durationformatted=null;
        if(Integer.parseInt(str_duration)<60)
        {
            Durationformatted=str_duration + "sec";
        }
        else
        {
            int min=Integer.parseInt(str_duration)/60;
            int sec=Integer.parseInt(str_duration)%60;

            if(sec==0)
            {
                Durationformatted=min+"min";
            }
            else
            {
                Durationformatted=min+"min" + sec +"sec";
            }

        }
        return  Durationformatted;
    }

    private String getFormateDateTime(String CallTime,String InputFormat,String OutFormat)
    {
        String formatteddate=CallTime;
        DateFormat inputFormat=new SimpleDateFormat(InputFormat, Locale.getDefault());
        DateFormat outputFormat=new SimpleDateFormat(OutFormat, Locale.getDefault());
        Date date=null;
        try
        {
            date=inputFormat.parse(CallTime);
        }catch (Exception e)
        {
        }
        if (date !=null)
        {
            formatteddate=outputFormat.format(date);
        }
        return formatteddate;
    }

    private void init(View v)
    {
        rv_activity_main_logs=v.findViewById(R.id.activity_main_logs);
        Swipe_logs=v.findViewById(R.id.Swipe_logs);

        rv_activity_main_logs.setLayoutManager(new LinearLayoutManager(getContext()));
        call_logs=new ArrayList<>();
        calllogAdapter=new CallLogAdapter(getContext(),call_logs);
        rv_activity_main_logs.setHasFixedSize(true);
        rv_activity_main_logs.setAdapter(calllogAdapter);

        Swipe_logs.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                FetchCallLogs();
                Swipe_logs.setRefreshing(false);
            }
        });
    }

    private boolean CheckPermission()
    {
        if(ActivityCompat.checkSelfPermission(getContext(),CalllogPermission)== PackageManager.PERMISSION_GRANTED)
        {
            return true;
        }
        else
        {
            ActivityCompat.requestPermissions(getActivity(), new String[]{CalllogPermission},PERMISSION_CODE);
            return false;
        }

    }
}