package com.intern.broadcastreceiver;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.io.File;
import java.util.ArrayList;

class CallLogAdapter extends RecyclerView.Adapter<CallLogAdapter.LogsViewHolder> {
    Context context;
    ArrayList<CalllogModal> call_logs;

    public CallLogAdapter(Context context, ArrayList<CalllogModal> call_logs) {
        this.context = context;
        this.call_logs = call_logs;
    }

    @Override
    public LogsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.calllogs_design, parent, false);
        return new LogsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LogsViewHolder holder, int position) {
        CalllogModal calllogModal=call_logs.get(position);
        holder.tvContactno.setText(calllogModal.contactName);
        holder.tvPhoneno.setText(calllogModal.phnumber);
        holder.tvTime.setText(calllogModal.callTime);
        holder.tvDate.setText(calllogModal.callDate);
        holder.tvDuration.setText(calllogModal.callDuration);
        holder.imageView2.setImageDrawable(context.getResources().getDrawable(R.drawable.download));
    }


    @Override
    public int getItemCount() {
        return call_logs.size();
    }

    public class LogsViewHolder extends RecyclerView.ViewHolder {
        TextView tvContactno,tvPhoneno,tvTime,tvDate,tvDuration;
        ImageButton CallImg;
        ImageView imageView2;
        public LogsViewHolder(@NonNull View itemView) {
            super(itemView);
            tvContactno=itemView.findViewById(R.id.tvContactno);
            tvPhoneno=itemView.findViewById(R.id.tvPhoneno);
            tvTime=itemView.findViewById(R.id.tvTime);
            tvDate=itemView.findViewById(R.id.tvDate);
            tvDuration=itemView.findViewById(R.id.tvDuration);
            CallImg=itemView.findViewById(R.id.CallImg);
            imageView2=itemView.findViewById(R.id.imageView2);

        }
    }
}
